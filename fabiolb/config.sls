# -*- coding: utf-8 -*-
# vim: ft=sls

{%- from "fabiolb/map.jinja" import fabiolb with context %}

fabiolb-config:
  file.managed:
    - name: {{ fabiolb.config_dir }}/fabio.properties
    - user: {{ fabiolb.user }}
    - group: {{ fabiolb.group }}
    - mode: 0640
    - contents: |
        # Managed by Salt
        {%- for property, value in fabiolb.config.iteritems() %}
        {%- for x in range(0, fabiolb.config[property]|length) %}
        {{ property }} = {{ value[x] }}
        {%- endfor %}
        {%- endfor %}
    - require:
      - file: {{ fabiolb.config_dir }}
    {%- if fabiolb.service != False %}
    - watch_in:
      - service: fabiolb-service
    {%- endif %}

# Enabling the service here to ensure each state is independent.
fabiolb-service:
  service.running:
    - name: fabiolb
    # Restart service if config changes
    - restart: True
    - enable: {{ fabiolb.service }}
