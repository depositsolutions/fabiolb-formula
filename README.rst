=============
fabiolb-formula
=============
.. image:: https://git.prod.infra.deposit/saltstack/fabiolb-formula/badges/master/build.svg
    :target: https://git.prod.infra.deposit/saltstack/fabiolb-formula/pipelines

A saltstack formula created to setup `Fabio Loadbalancer
<https://fabiolb.net>`_ on a single machine.

.. notes::

    It is mandatory to have Consul installed .
    Fabio must run as root for it to be able to use reserved ports (HTTP / HTTPS).


Available states
================

.. contents::
    :local:

``init``
--------

The essential Nomad state running both ``install`` and ``config``.

``install``
------------

Install the Fabio package GO binary, and register the appropriate service
definition with systemd.

This state can be called independently.

``config``
-----------

Deploy the configuration files required to run the service, and enable the
service if configured to do so.

Configuration is done through the ``fabiolb`` pillar.

This state can be called independently.

``uninstall``
-------------

Remove the service, binaries, and configuration files. The data itself will be kept and needs
to be removed manually, just to be on the safe side.

This is state must always be called independently.
